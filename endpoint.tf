resource "aws_s3_bucket_object" "rdsendpoint" {
  bucket = "omartinex"
  key    = "rdsendpoint.txt"
  content = "${aws_db_instance.omxdb.address}"
}