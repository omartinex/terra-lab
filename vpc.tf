resource "aws_vpc" "omxvpc" {
    cidr_block = "${var.vpc_cidr}"
    enable_dns_hostnames = true
    tags {
        Name = "terraform-omx-vpc"
    }
}

resource "aws_internet_gateway" "omxigw" {
    vpc_id = "${aws_vpc.omxvpc.id}"
}

/*
  NAT Gateway
*/

resource "aws_nat_gateway" "omxngw" {
  allocation_id = "${aws_eip.nat.id}"
  subnet_id     = "${aws_subnet.eu-west-2a-public.id}"
  depends_on = ["aws_internet_gateway.omxigw"]
}

resource "aws_eip" "nat" {
  vpc = true
}

/*
  Public Subnet
*/
resource "aws_subnet" "eu-west-2a-public" {
    vpc_id = "${aws_vpc.omxvpc.id}"
    map_public_ip_on_launch = true
    cidr_block = "${var.public_subnet_cidr}"
    availability_zone = "eu-west-2a"

    tags {
        Name = "public_subnet"
    }
}

resource "aws_route_table" "eu-west-2a-public" {
    vpc_id = "${aws_vpc.omxvpc.id}"

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = "${aws_internet_gateway.omxigw.id}"
    }

    tags {
        Name = "route_table_public_subnet"
    }
}

resource "aws_route_table_association" "eu-west-2a-public" {
    subnet_id = "${aws_subnet.eu-west-2a-public.id}"
    route_table_id = "${aws_route_table.eu-west-2a-public.id}"
}

/*
  Private Subnet
*/
resource "aws_subnet" "eu-west-2b-private" {
    vpc_id = "${aws_vpc.omxvpc.id}"

    cidr_block = "${var.private_subnet_b_cidr}"
    availability_zone = "eu-west-2b"

    tags {
        Name = "private_subnet_b"
    }
}

resource "aws_route_table" "eu-west-2b-private" {
    vpc_id = "${aws_vpc.omxvpc.id}"

    route {
        cidr_block = "0.0.0.0/0"
        nat_gateway_id = "${aws_nat_gateway.omxngw.id}"
    }

    tags {
        Name = "route_table_private_subnet_b"
    }
}

resource "aws_route_table_association" "eu-west-2b-private" {
    subnet_id = "${aws_subnet.eu-west-2b-private.id}"
    route_table_id = "${aws_route_table.eu-west-2b-private.id}"
}

resource "aws_subnet" "eu-west-2a-private" {
    vpc_id = "${aws_vpc.omxvpc.id}"

    cidr_block = "${var.private_subnet_a_cidr}"
    availability_zone = "eu-west-2a"

    tags {
        Name = "private_subnet_a"
    }
}

resource "aws_route_table" "eu-west-2a-private" {
    vpc_id = "${aws_vpc.omxvpc.id}"

    route {
        cidr_block = "0.0.0.0/0"
        nat_gateway_id = "${aws_nat_gateway.omxngw.id}"
    }

    tags {
        Name = "route_table_private_subnet_a"
    }
}

resource "aws_route_table_association" "eu-west-2a-private" {
    subnet_id = "${aws_subnet.eu-west-2a-private.id}"
    route_table_id = "${aws_route_table.eu-west-2a-private.id}"
}
