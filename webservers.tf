resource "aws_instance" "omxwebsrv1"{
	ami = "ami-b6daced2"
	instance_type = "t2.micro"
	subnet_id = "${aws_subnet.eu-west-2a-public.id}"
	vpc_security_group_ids = ["${aws_security_group.sgweb.id}"]
	key_name = "key"
	depends_on = ["aws_db_instance.omxdb"]
	iam_instance_profile = "S3-Admin-Access"
	user_data = "${file("userdata.web")}"
	tags{
		Name = "omxwebsrv1"
	}
}
resource "aws_instance" "omxwebsrv2"{
	ami = "ami-b6daced2"
	instance_type = "t2.micro"
	subnet_id = "${aws_subnet.eu-west-2a-public.id}"
	vpc_security_group_ids = ["${aws_security_group.sgweb.id}"]
	key_name = "key"
	depends_on = ["aws_db_instance.omxdb"]
	iam_instance_profile = "S3-Admin-Access"
	user_data = "${file("userdata.web")}"
	tags{
		Name = "omxwebsrv2"
	}
}
