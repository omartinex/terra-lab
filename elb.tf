# Create a new load balancer
resource "aws_elb" "elb1" {
  name               = "terra-elb-omartinex"
  security_groups = ["${aws_security_group.sgweb.id}"]
  subnets = ["${aws_subnet.eu-west-2a-public.id}"]

  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:80/health.html"
    interval            = 5
  }

  instances                   = ["${aws_instance.omxwebsrv1.id}"]
  cross_zone_load_balancing   = true
  idle_timeout                = 400
  connection_draining         = true
  connection_draining_timeout = 400

  instances                   = ["${aws_instance.omxwebsrv2.id}"]
  cross_zone_load_balancing   = true
  idle_timeout                = 400
  connection_draining         = true
  connection_draining_timeout = 400

  tags {
    Name = "terra-elb-omartinex.com"
  }
}
