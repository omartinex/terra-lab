USE omxdb;

CREATE TABLE info_persona (
        id INT NOT NULL AUTO_INCREMENT,
        nombre VARCHAR(50) NOT NULL,
        edad INT NOT NULL,
        ciudad VARCHAR(50) NOT NULL,
        PRIMARY KEY  (id)
);