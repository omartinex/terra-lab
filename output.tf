output "sgrds" {
  value = "${aws_security_group.sgrds.id}"
}

output "sgweb" {
  value = "${aws_security_group.sgweb.id}"
}

output "rdsendpoint"{
	value = "${aws_db_instance.omxdb.address}"
}

output "omxwebsrv1"{
	value = "${aws_instance.omxwebsrv1.public_ip}"
}

output "omxwebsrv2"{
	value = "${aws_instance.omxwebsrv2.public_ip}"
}

output "elb1"{
	value = "${aws_elb.elb1.dns_name}"
}

output "nateip"{
	value = "${aws_eip.nat.id}"
}

output "subnetid"{
	value = "${aws_subnet.eu-west-2a-public.id}"
}

output "omxvpcid"{
	value = "${aws_vpc.omxvpc.id}"
}

output "omxsubnetgrp"{
	value = "${aws_db_subnet_group.omxsubnetgrp.id}"
}
